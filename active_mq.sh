#!/usr/bin/env bash

function update_activemq {
    local choice_arg=${1}
    
    local version_reg_exp="[0-9]+\.[0-9]+\.[0-9]+"

    function get_current_version {
        local _return=${1:?${FUNCNAME} require return_ argument}
        local cmd=activemq        
        if ! which ${cmd}
            then
                echo "[WARN] there is no ${cmd} available"
                eval $_return="UNKNOWN"            
                return 1
            else
                eval $_returl=`${cmd} --version | grep -oP "(?<=^ActiveMQ )${version_reg_exp}"`
                return 0
        fi
    }    
    
    function get_last_version {
        local _return=${1:?${FUNCNAME} require return_ argument}
        eval $_return=`curl --silent "http://activemq.apache.org/download.html" | egrep -o "ActiveMQ ${version_reg_exp} Release</a></p>" | egrep -o ${version_reg_exp}`
    }
    
    function download {
        local version=${1}        
        local base_path=${PPA_HOME:=${HOME}/ppa-dir}/apache-activemq-${version}
        
        echo "[INFO] download into '${base_path}'"
        local url=$(curl --silent "http://www.apache.org/dyn/closer.cgi?path=/activemq/${version}/apache-activemq-${version}-bin.tar.gz" | grep -oP "(?<=href=\").*?apache-activemq-${version}-bin.tar.gz" | head -n1)
        mkdir -p ${PPA_HOME}
        
        curl "${url}" > ${base_path}.tar.gz
        tar -xvf ${base_path}.tar.gz -C ${PPA_HOME}
        chmod +x ${base_path}/bin/activemq
        chmod +x ${base_path}/bin/activemq-admin
        rm ${base_path}.tar.gz
    }
    
    function install {
        if ! ${PPA_INSTALL:-true}
            then
                echo "[INFO] ignore install"
                return 1
        fi
        local version=${1}        
        
        echo "[INFO] install"
        
        echo "" >> ~/.bashrc        
        echo "# ppa upgrade `date`" >> ~/.bashrc        
        echo "export ACTIVEMQ_HOME=\"${PPA_HOME}/apache-activemq-${version}\"" >> ~/.bashrc
        echo "export PATH=\${ACTIVEMQ_HOME}/bin:\${PATH}" >> ~/.bashrc
        echo "please reload bash: . ~/.bashrc"
    }    
    
    echo "[INFO] force reload bash"
    . ~/.bashrc
    
    local current_version=""
    get_current_version current_version
    echo "[INFO] CURRENT version of activemq is ${current_version}"
    
    local last_version=""
    get_last_version last_version
    echo "[INFO] LAST version of activemq is ${last_version}"   
    
             
    if [[ ${current_version} = "UNKNOWN" || ${last_version} > ${current_version} ]]
        then
            echo "[INFO] There is new version available of activemq ${last_version}"
            while true
            do
                if [[ ${choice_arg} = "-y" ]];
                    then
                        choice="y"
                    else
                        read -p "updgrade (y/n)?" choice
                        if [[ $? != 0 ]]; then 
                            choice="n"
                            echo "please set explicit -y/n"
                        fi
                fi                
                case "$choice" in 
                  y|Y ) 
                        download ${last_version}
                        install ${last_version}
                        break
                        ;;
                  n|N ) echo "no"
                        break
                        ;;
                  * ) echo "invalid"
                        ;;
                esac
            done
            
        else
            echo "[INFO] Your version of activemq is up to date ${last_version}"
    fi
}

update_activemq $1

# export PPA_DIR=/tmp/faf
# export PPA_INSTALL=false
# active_mq.sh -y 