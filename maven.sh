#!/usr/bin/env bash

function update_maven {
    local choice_arg=${1}
    
    local version_reg_exp="[0-9]+\.[0-9]+\.[0-9]+"

    function get_current_version {
        mvn --version | grep -oP "(?<=^Apache Maven )${version_reg_exp}"
    }    
    
    function get_last_version {
        curl --silent "http://maven.apache.org/download.cgi" | egrep -o "Download Apache Maven ${version_reg_exp}</h2>" | egrep -o ${version_reg_exp}                        
    }
    
    function download {
        local version=${1}
        local base_path=${PPA_DIR_HOME:=${HOME}/ppa-dir}/apache-maven-${version}
        
        echo "download"
        local url=$(curl --silent "http://maven.apache.org/download.cgi" | grep -oP "<a.*?${version}-bin.tar.gz\"" | cut -d\" -f2)        
        mkdir -p ${PPA_DIR_HOME}
        
        curl "${url}" > ${base_path}.tar.gz
        tar -xvf ${base_path}.tar.gz -C ${PPA_DIR_HOME}
        chmod +x ${base_path}/bin/mvn
        rm ${base_path}.tar.gz
    }
    
    function install {
        local version=${1}
        echo "install"
        
        echo "" >> ~/.bashrc        
        echo "# ppa upgrade `date`" >> ~/.bashrc        
        echo "export MAVEN_HOME=\"${PPA_DIR_HOME}/apache-maven-${version}\"" >> ~/.bashrc
        echo "export PATH=\${MAVEN_HOME}/bin:\${PATH}" >> ~/.bashrc
        echo "please reload bash: . ~/.bashrc"
    }
    
    echo "force reload bash"
    . ~/.bashrc
    
    local current_version=$(get_current_version)
    echo "CURRENT version of maven is ${current_version:-UNKNOWN}"
    
    local last_version=$(get_last_version) 
    echo "LAST version of maven is ${last_version}"   
    
             
    if [[ ${last_version} > ${current_version} ]]
        then
            echo "There is new version available of maven ${last_version}"
            while true
            do
                if [[ ${choice_arg} = "-y" ]];
                    then
                        choice="y"
                    else
                        read -p "updgrade (y/n)?" choice
                        if [[ $? != 0 ]]; then 
                            choice="n"
                            echo "please set explicit -y/n"
                        fi
                fi                
                case "$choice" in 
                  y|Y ) 
                        download ${last_version}
                        install ${last_version}
                        break
                        ;;
                  n|N ) echo "no"
                        break
                        ;;
                  * ) echo "invalid"
                        ;;
                esac
            done
            
        else
            echo "Your version of maven is up to date ${last_version}"
    fi
}

update_maven $1
