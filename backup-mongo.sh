#!/usr/bin/env bash

#---------------
# usage:

# via cronjob
# /etc/cron.d/backup-mongo
# run backup every day at 6 o'clock by user bkp-mng for databases test testA testB.
# 0 6 * * *  bkp-mng /path/to/backup-mongodb.sh test testA testB >> /path/to/BACKUP_MONGO_FOLDER/backup_mongodb.cron.log 2>&1

# via curlbash
# curl https://bitbucket.org/ada/ppa-dir.com/raw/23693cb3f2c5d12be8e02c1244f3502a07431a07/backup-mongodb.sh | bash /dev/stdin 6 testA testB >> /path/to/BACKUP_MONGO_FOLDER/backup_mongodb.cron.log 2>&1
#---------------


# check all viariables
echo ${BACKUP_MONGO_FOLDER:?}           > /dev/null # /external/storage/mongo
echo ${BACKUP_MONGO_HOST:?}             > /dev/null # 127.0.0.1
echo ${BACKUP_MONGO_PORT:?}             > /dev/null # 27017
echo ${BACKUP_MONGO_DUMP_NAME_PREFIX:?} > /dev/null # mongo-production
echo ${BACKUP_MONGO_KEEP_DUMP_DAYS:?}   > /dev/null # 2
echo ${BACKUP_MONGO_KEEP_LOG_DAYS:?}    > /dev/null # 2  

echo ""
echo "========"
echo "****************************"
echo "START NEW BACKUP AT `date`"
echo "****************************"

function do_mongo_backup() {
    local name=${1}
    local dated_name=${BACKUP_MONGO_DUMP_NAME_PREFIX}-${name}-`date +%h-%d-%Y`
    echo ""
    echo "-"
    echo "- - - - - BACKUPING ${name}"
    echo "-"

    echo "make dump"
    mongodump \
        --host ${BACKUP_MONGO_HOST} \
        --port ${BACKUP_MONGO_PORT} \
        -d ${name} \
        -o ${BACKUP_MONGO_FOLDER}
        
    echo "after dump wait 30 sec"
    sleep 30

    echo "rename dump"
    mv ${name} ${dated_name}
    echo "after rename dump wait 1 min"
    sleep 60

    echo "start archive '${name}' at `date`"
    tar -jcvf ${dated_name}.tar.bz2 ${dated_name}/
    local archive_errr_code=$?
    echo "end archive '${name}' at `date`" 

    if [ ${archive_errr_code} -ne 0 ];
        then
            echo "during archive of '${dated_name}' was detected exception with code '${archive_errr_code}'"
        else
            echo "archive of '${dated_name}' pass succesefull, going to remove dump and leave only archive"
            # save hashprint into log also
            md5sum "${dated_name}.tar.bz2"
            rm -r ${dated_name}
    fi

    echo "going to delete all file older than ${BACKUP_MONGO_KEEP_DUMP_DAYS} day for file name pattern '${BACKUP_MONGO_DUMP_NAME_PREFIX}-${name}-"
    find ${BACKUP_MONGO_DUMP_NAME_PREFIX}-${name}-* -mtime +${BACKUP_MONGO_KEEP_DUMP_DAYS} -type f -delete
}

function rename_log {
    local log=${1}    
    echo
    echo "rename log file ${LOG_FILE} into ${log}.`date +%h-%d-%Y`"
    cat "${log}" > "${log}.`date +%h-%d-%Y`"
    echo "prev backup have be done at `date` into ${log}.`date +%h-%d-%Y`" > "${log}"
    find ${log}.* -mtime +${BACKUP_MONGO_KEEP_LOG_DAYS} -type f -delete
}   

cd ${BACKUP_MONGO_FOLDER}

for name in $*
do
    do_mongo_backup $name
done

rename_log ${BACKUP_MONGO_FOLDER}/backup_mongodb.cron.log






