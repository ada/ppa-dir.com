#!/usr/bin/env bash

function update_tomcat {
    local major_version_arg=${1}
    local choice_arg=${2}
    
    local version_reg_exp="[0-9]+\.[0-9]+\.[0-9]+"

    function get_current_version {
        local _return=${1:?${FUNCNAME} require return_ argument}
        local cmd=catalina.sh        
        if ! which ${cmd}
            then
                eval $_return="UNKNOWN"
                echo "[WARN] there is no ${cmd} available"
                return 1
            else
                eval $_returl=`${cmd} version | grep -oP "(?<=^Server version: Apache Tomcat/)${version_reg_exp}"`
                return 0
        fi
    }    
    
    function get_last_version {
        local _return=${1:?${FUNCNAME} require return_ argument}
        eval $_return=`curl --silent "http://tomcat.apache.org/download-${major_version_arg}0.cgi" | grep -oP "${version_reg_exp}(?=</h3>)"`
    }
    
    function download {
        local version=${1}

        local base_path=${PPA_HOME:=${HOME}/ppa-dir}/apache-tomcat-${version}
        
        echo "download into '${base_path}'"
        local url=$(curl --silent "http://tomcat.apache.org/download-${1:0:1}0.cgi" | grep -oP "<a.*?${version}.tar.gz\"" | cut -d\" -f2)        
        mkdir -p ${PPA_HOME}
        
        curl "${url}" > ${base_path}.tar.gz
        tar -xvf ${base_path}.tar.gz -C ${PPA_HOME}
        chmod +x ${base_path}/bin/*.sh
        rm ${base_path}.tar.gz
    }
    
    function install {
        if ! ${PPA_INSTALL:-true}
            then
                echo "ignore install"
                return 1            
        fi
             
        local version=${1}
        echo "install"
        
        echo "" >> ~/.bashrc        
        echo "# ppa upgrade `date`" >> ~/.bashrc        
        echo "export CATALINA_HOME=\"/home/\${USER}/ppa-dir/apache-tomcat-${version}\"" >> ~/.bashrc
        echo "export PATH=\${CATALINA_HOME}/bin:\${PATH}" >> ~/.bashrc
        echo "please reload bash: . ~/.bashrc"
    }
    
    echo "force reload bash"
    . ~/.bashrc
    
    local current_version=""
    get_current_version current_version
    echo "CURRENT version of tomcat is ${current_version}"
    
    local last_version=""
    get_last_version last_version     
    echo "LAST version of tomcat is ${last_version}"    
             
    if [[ ${current_version} = "UNKNOWN" || ${last_version} > ${current_version} ]]
        then
            echo "There is new version available of tomcat ${last_version}"
            while true
            do
                if [[ ${choice_arg} = "-y" ]];
                    then
                        choice="y"
                    else
                        read -p "updgrade (y/n)?" choice
                        if [[ $? != 0 ]]; then 
                            choice="n"
                            echo "please set explicit -y/n"
                        fi
                fi                
                case "$choice" in 
                  y|Y ) 
                        download ${last_version}
                        install ${last_version}                        
                        break
                        ;;
                  n|N ) echo "no"
                        break
                        ;;
                  * ) echo "invalid"
                        ;;
                esac
            done
            
        else
            echo "Your version of maven is up to date ${last_version}"
    fi
}

if [[ ${1} =~ ^[6-8]$ ]]
    then
        echo "going to udate tomcat ${1}"
    else
        printf "expecting:\n ${0} \${major_version:6, 7 or 8}\n"
        exit 1
fi

update_tomcat ${1} ${2}

# export PPA_DIR=/tmp/faf
# export PPA_INSTALL=false
# active_mq.sh 6 -y 